import { Box, Button, Container, Grid, Typography } from "@mui/material";
import React from "react";
import { MdArrowOutward } from "react-icons/md";


const HomePageCard = ({ cardIcon, cardTitle, cardParagraph }) => {
    return (
        <Box className="home-page-card-background">
            <Grid container>
                <Grid item xs={3}>
                    <Box className="card-icon">
                        {cardIcon}
                    </Box>
                </Grid>
                <Grid item xs={9}>

                </Grid>
            </Grid>
            <Box className="body-content-small-margin">
                <Container>
                    <Typography className="card-title">{cardTitle}</Typography>
                    <Typography className="card-paragraph">{cardParagraph}</Typography>
                    <Button className="card-button">Read More <MdArrowOutward size={18} style={{ marginLeft: "3px" }} /></Button>
                </Container>
            </Box>
        </Box>
    )
}

export default HomePageCard;