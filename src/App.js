import Routing from './routes/routes';
import './static/css/style.css';

function App() {
  return (
    <>
      <Routing />
    </>
  );
}

export default App;
