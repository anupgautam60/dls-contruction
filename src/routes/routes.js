import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "../container/template/pages/homepage";


const Routing = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
            </Routes>
        </BrowserRouter>
    )
}

export default Routing;