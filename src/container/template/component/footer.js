import { Box, Container, Grid, Typography } from "@mui/material";
import React from "react";
import Logo from "../../../static/image/logo.png"
import { FaFacebookF, FaTwitter, FaLinkedinIn, FaYoutube, FaArrowRight } from "react-icons/fa";
import { Link } from "react-router-dom";



const Footer = () => {
    return (
        <Box className="body-content-margin">
            <Box className="footer-background-color">
                <Container>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <Grid container>
                                <Grid item xs={7}>
                                    <Box className="home-page-link">
                                        <img src={Logo} className="logo-image" />
                                        <Typography className={"home-page-link-text-white"}>DLS Contruction</Typography>
                                    </Box>
                                </Grid>
                                <Grid item xs={5}>

                                </Grid>
                            </Grid>
                            <Typography className="card-paragraph">Sapien luctus lesuada sentus pharetra nisilona quisuea aenean eros mus magnis arcu vehicula nascetur feugiat service.</Typography>
                            <Box className="home-page-link">
                                <Box className={'footer-icon-design'}>
                                    <FaFacebookF size={18} />
                                </Box>
                                <Box className={'footer-icon-design'}>
                                    <FaTwitter size={18} />
                                </Box>
                                <Box className={'footer-icon-design'}>
                                    <FaLinkedinIn size={18} />
                                </Box>
                                <Box className={'footer-icon-design'}>
                                    <FaYoutube size={18} />
                                </Box>
                            </Box>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container spacing={3}>
                                <Grid item xs={3.5}>
                                    <Box >
                                        <Typography className={"home-page-link-text-white"}>USEFUL LINKS</Typography>
                                        <Box style={{ marginTop: "25px" }}>
                                            <ul style={{ padding: '0' }}>
                                                <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}About Us</Link></li>
                                                <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Our Gallery</Link></li>
                                                <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Our Services</Link></li>
                                                <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Our Team</Link></li>
                                                <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Contact Us</Link></li>
                                            </ul>
                                        </Box>
                                    </Box>
                                </Grid>
                                <Grid item xs={3.5}>
                                    <Typography className={"home-page-link-text-white"}>OUR SERVICES</Typography>
                                    <Box style={{ marginTop: "25px" }}>
                                        <ul style={{ padding: '0' }}>
                                            <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Renovation</Link></li>
                                            <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Contruction</Link></li>
                                            <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Architectural</Link></li>
                                            <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Development</Link></li>
                                            <li style={{ listStyle: 'none', marginBottom: "12px" }}><Link to="#" className="footer-link"><FaArrowRight size={15} style={{ marginRight: "10px" }} /> {'  '}Building</Link></li>
                                        </ul>
                                    </Box>
                                </Grid>
                                <Grid item xs={5}>
                                    <Typography className={"home-page-link-text-white"}>NEWSLETTER</Typography>
                                    <Box style={{ marginTop: "25px", marginLeft: "10px" }}>
                                        <Typography className="card-paragraph">Aplications prodize before front end ortals visualize front end</Typography>
                                        <Box className="footer-form-design">
                                            <Grid container>
                                                <Grid item xs={7.8}>
                                                    <input type="text" className="form-design" placeholder="Email..." />
                                                </Grid>
                                                <Grid item xs={4.2}>
                                                    <input type="submit" value={'Subscribe'} className="form-button"  />
                                                </Grid>
                                            </Grid>
                                        </Box>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </Box>
    )
}

export default Footer;