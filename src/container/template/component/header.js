import { Box, Container, Grid, List, ListItem, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Logo from "../../../static/image/logo.png"



const Header = () => {
    const [scrollBg, setScrollBg] = useState(false);

    console.log("scrollBg", scrollBg);

    useEffect(() => {
        const handleScroll = () => {
            const scrollPosition = window.scrollY;
            if (scrollPosition > window.innerHeight * 0.2) {
                setScrollBg(true);
            } else {
                setScrollBg(false);
            }
        };

        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);
    return (
        <Box>
            <Box className={scrollBg ? "main-header-white" : "main-header"} >
                <Box className="main-header-border">
                    <Container>
                        <Grid container>
                            <Grid item md={2.3}>
                                <Box className="home-page-link">
                                    <img src={Logo} className="logo-image" />
                                    <Typography className={scrollBg ? "home-page-link-text-white" : "home-page-link-text"}>DLS Contruction</Typography>
                                </Box>
                            </Grid>
                            <Grid item md={9.7}>
                                <Box>
                                    <ul >
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Our Company</Link></li>
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Our Services</Link></li>
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Our Projects</Link></li>
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>News & Insights</Link></li>
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Carreers</Link></li>
                                        {/* <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Bars</Link></li> */}
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Contact Us</Link></li>
                                        <li className="list-link"><Link to="/" className={scrollBg ? "link-item link-item-white" : "link-item"}>Become a Subcontractor</Link></li>
                                    </ul>
                                </Box>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </Box >
    )
}

export default Header;