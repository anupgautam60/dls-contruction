import { Box, Button, Container, Grid, Typography } from "@mui/material";
import React from "react";
import Header from "../component/header";
import Footer from "../component/footer";
import LandingPageVideo from "../../../static/image/dls-landing-page-video.mp4";
import { LiaLongArrowAltRightSolid } from "react-icons/lia";
import HomePageCard from "../../../component/homePageCard";
import { IoRocketOutline } from "react-icons/io5";
import { LiaFontAwesomeFlag } from "react-icons/lia";
import { MdOutlineMenuBook } from "react-icons/md";
import AboutImage from "../../../static/image/about_img.jpg"
import { MdArrowOutward } from "react-icons/md";
import { TbUserStar } from "react-icons/tb";
import { FaBridge } from "react-icons/fa6";
import NewsImage from "../../../static/image/news&images.jpeg"
import NewsImage1 from "../../../static/image/news&images1.jpeg"
import NewsImage2 from "../../../static/image/news&images2.jpeg"
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <Box>
            <Header />
            <HomePage />
            <Footer />
        </Box>
    )
}

const HomePage = () => {
    return (
        <Box>
            <Box className="video-backgound-color"></Box>
            <Box className="video-effect">
                <video autoPlay muted loop playsinline className="landing-page-video-mp4">
                    <source src={LandingPageVideo} type="video/mp4" />
                </video>
            </Box>
            <Box>
                <Container>
                    <Box className="body-main-content">
                        <Grid item md={4}>
                            <Typography className="body-content-text">Building What <br /> <span className="body-context-text-style">Matters</span> To You</Typography>
                        </Grid>
                        <Box style={{ marginTop: '25px', marginLeft: "25px", width: "100%" }}>
                            <Box style={{ display: 'flex' }}>
                                <Typography className="body-content-small-text">WHAT DO YOU WANT TO BUILD?</Typography>
                                <LiaLongArrowAltRightSolid size={40} className="right-arrow" />
                            </Box>
                          
                        </Box>
                    </Box>
                    <Box className="body-content-margin">
                        <Grid container spacing={4}>
                            <Grid item xs={4}>
                                <HomePageCard
                                    cardIcon={<IoRocketOutline size={45} />}
                                    cardTitle={'Look’s Our Mission'}
                                    cardParagraph={'Nullam tortor rhoncus nisl tristique velit taciti platea eu rutrum ridiculus, elementum.'}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <HomePageCard
                                    cardIcon={<LiaFontAwesomeFlag size={46} />}
                                    cardTitle={'Team Core Values'}
                                    cardParagraph={'Nullam tortor rhoncus nisl tristique velit taciti platea eu rutrum ridiculus, elementum.'}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <HomePageCard
                                    cardIcon={<MdOutlineMenuBook size={46} />}
                                    cardTitle={'About Our Story'}
                                    cardParagraph={'Nullam tortor rhoncus nisl tristique velit taciti platea eu rutrum ridiculus, elementum.'}
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box className="body-content-margin">
                        <Grid container spacing={7}>
                            <Grid item xs={5.5}>
                                <Box className="about-background">
                                    <img src={AboutImage} style={{ width: "100%" }} alt="logo-image" />
                                    <Box className="about-image-content">
                                        <Grid container spacing={2}>
                                            <Grid item xs={7}>
                                                <Typography className="image-title">Let's Work Together</Typography>
                                            </Grid>
                                            <Grid item xs={5}>
                                                <Button className="card-button" style={{ width: "100%" }}>Contact Us <MdArrowOutward size={18} style={{ marginLeft: "3px" }} /></Button>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Box>
                            </Grid>
                            <Grid item xs={6.5}>
                                <Box style={{ position: 'relative' }}>
                                    <Typography className="about-title">DELIVERING OUR CLIENTS MORE PROJECT CLARITY...</Typography>
                                    <Typography className="about-title-position">01</Typography>
                                </Box>
                                <Box>
                                    <Typography className="about__content">Mus gravida tellusras nibh dapibus bibendum litora phasellus curabit natoque conubia suspendisse potenti viverra vehicula, vulputate dignissim netus urna dis quisque used volutpat consequat fermentum.</Typography>
                                </Box>
                                <Box>
                                    <Grid container spacing={5}>
                                        <Grid item xs={6}>
                                            <Box className="about-border-bottom">
                                                <Box className='about-icon-design'>
                                                    <Box className="about-icon-flip">
                                                        <Box className="about-flip-front">
                                                            <TbUserStar />
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </Box>
                                            <Box style={{ marginTop: '20px' }}>
                                                <Typography className="about-card-title">We’ve Expert Members</Typography>
                                                <Typography className="card-paragraph">sollicitudin primis Class ad porta tempus per him accumsan posuer felis sollicitudin primis malesuada ullamcorper duis. Class ad porta tempus per him accumsan posuer felis sollicitudin primis malesuada ullamcorper duis.</Typography>
                                            </Box>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <Box className="about-border-bottom">
                                                <Box className='about-icon-design'>
                                                    <Box className="about-icon-flip">
                                                        <Box className="about-flip-front">
                                                            <FaBridge />
                                                        </Box>
                                                    </Box>
                                                </Box>
                                            </Box>
                                            <Box style={{ marginTop: '20px' }}>
                                                <Typography className="about-card-title">Bridge Construction</Typography>
                                                <Typography className="card-paragraph">sollicitudin primis Class ad porta tempus per him accumsan posuer felis sollicitudin primis malesuada ullamcorper duis. Class ad porta tempus per him accumsan posuer felis sollicitudin primis malesuada ullamcorper duis.</Typography>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box className="body-content-margin">
                        <Box style={{ position: 'relative' }}>
                            <Typography className="about-title">News & Insights</Typography>
                            <Typography className="about-title-position">02</Typography>
                        </Box>
                        <Box style={{ marginTop: "3rem" }}>
                            <Grid container spacing={4}>
                                <Grid item xs={4}>
                                    <img src={NewsImage} className="news-image-design" alt="news" />
                                    <Box style={{ marginTop: "15px" }}>
                                        <Typography className="news-event-title">Turner Selected to Build UC Merced New Medical Education Building</Typography>
                                        <Link to="#" className="read-more-button">READ MORE <LiaLongArrowAltRightSolid size={25} className="read-more-button-icon" /></Link>
                                    </Box>
                                </Grid>
                                <Grid item xs={4}>
                                    <img src={NewsImage1} className="news-image-design" alt="news" />
                                    <Box style={{ marginTop: "15px" }}>
                                        <Typography className="news-event-title">Turner Breaks Ground on the California</Typography>
                                        <Link to="#" className="read-more-button">READ MORE <LiaLongArrowAltRightSolid size={25} className="read-more-button-icon" /></Link>
                                    </Box>
                                </Grid>
                                <Grid item xs={4}>
                                    <img src={NewsImage2} className="news-image-design" alt="news" />
                                    <Box style={{ marginTop: "15px" }}>
                                        <Typography className="news-event-title">Turner Honored for Inclusivity, Diversity</Typography>
                                        <Link to="#" className="read-more-button">READ MORE <LiaLongArrowAltRightSolid size={25} className="read-more-button-icon" /></Link>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Container>
            </Box>
        </Box>
    )
}

export default Home;
